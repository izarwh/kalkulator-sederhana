<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//
class web extends Controller
{
    public function index(){
        return view('kalkulasiinput');
    }

    public function calculation(Request $request){
        $data = [
            'operasi' => $request->post('survey_options')
        ];
        return view('kalkulasi',$data);
    }
}
